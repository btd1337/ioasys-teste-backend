## Descrição

Projeto de avaliação para desenvolvedores Node.js.

## Instalação

```bash
$ npm install
```

## Pré-configuração

Crie na raiz do projeto um arquivo `.env` com a seguinte configuração:

```
EXPRESS_PORT=3000
EXPRESS_ENVIRONMENT=Development

TYPEORM_HOST=imdb-database
TYPEORM_DATABASE=db_imdb
TYPEORM_USERNAME=db_admin
TYPEORM_PASSWORD=SUA_SENHA
TYPEORM_PORT=5432
```

## Rodando a aplicação

```bash
# inicializando os containers
$ docker-compose up

# rodando as migrações
$ docker exec -it imdb-app npm run typeorm:run
```

## Documentação da API

Com a aplicação rodando, abra o seu navegador em:

http://localhost:3000/docs

### Arquivo de Coleção do Postman

Abra a pasta `collections` e:

1. Importe para o `Postman` a coleção `IMDB API.postman_collection.json`.

2. Importe o environment `IMDB.postman_environment.json`.

## License

This project is [GPL v3 licensed](LICENSE).
