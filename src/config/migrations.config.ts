import { ConnectionOptions } from 'typeorm';

const connectionOptions: ConnectionOptions = {
	type: 'postgres',
	host: process.env.TYPEORM_HOST,
	database: process.env.TYPEORM_DATABASE,
	username: process.env.TYPEORM_USERNAME,
	password: process.env.TYPEORM_PASSWORD,
	port: parseInt(process.env.TYPEORM_PORT, 10),
	logging: process.env.TYPEORM_LOGGING === 'true',
	entities: [`${__dirname}/../**/*.entity.{ts,js}`],
	migrationsRun: process.env.TYPEORM_MIGRATIONS_RUN === 'true',
	synchronize: false,
	dropSchema: false,
	migrations: ['src/migrations/**/*{.ts,.js}'],
	cli: {
		migrationsDir: 'src/migrations',
	},
};

export = connectionOptions;
