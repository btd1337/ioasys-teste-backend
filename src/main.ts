import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { CrudConfigService } from '@nestjsx/crud';
import { BUILD_FOLDER, API_FILE } from './config/constants';
import * as fs from 'fs';
import * as helmet from 'helmet';
import * as rateLimit from 'express-rate-limit';

CrudConfigService.load({
	query: {
		limit: 25,
		cache: 2000,
		maxLimit: 100
	},
	params: {
		id: {
			field: 'id',
			type: 'uuid',
			primary: true,
		},
	},
	routes: {
		updateOneBase: {
			allowParamsOverride: true,
		},
		deleteOneBase: {
			returnDeleted: true,
		},
	},
});

import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('IMDB API')
    .setDescription('Desafio Ioasys Node.js')
    .setVersion('1.0')
    .setContact('Helder Bertoldo', 'https://github.com/btd1337', 'helder.bertoldo@gmail.com')
    .addServer("http://")
	.addBearerAuth()
	.addTag('auth')
	.addTag('admins')
	.addTag('users')
	.addTag('actors')
	.addTag('directors')
	.addTag('movies')
	.addTag('votes')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  fs.writeFileSync(`${BUILD_FOLDER}/${API_FILE}`, JSON.stringify(document));
  SwaggerModule.setup('docs', app, document);
  
  app.use(helmet());
  app.use(
	rateLimit({
	  windowMs: 15 * 60 * 1000, // 15 minutes
	  max: 100, // limit each IP to 100 requests per windowMs
	}),
  );
  app.enableCors();
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(3000);
}
bootstrap();
