import {MigrationInterface, QueryRunner} from "typeorm";

export class createUsersTable1607321373571 implements MigrationInterface {
    name = 'createUsersTable1607321373571'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying(32) NOT NULL, "lastName" character varying(128) NOT NULL, "email" character varying(254) NOT NULL, "isDeleted" boolean NOT NULL DEFAULT false, "password" character varying NOT NULL, "salt" character varying NOT NULL, CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "user"`);
    }

}
