import {MigrationInterface, QueryRunner} from "typeorm";

export class createAdminsTable1607381145161 implements MigrationInterface {
    name = 'createAdminsTable1607381145161'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "admin" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying(32) NOT NULL, "lastName" character varying(128) NOT NULL, "email" character varying(254) NOT NULL, "isDeleted" boolean NOT NULL DEFAULT false, "password" character varying NOT NULL, "salt" character varying NOT NULL, CONSTRAINT "UQ_de87485f6489f5d0995f5841952" UNIQUE ("email"), CONSTRAINT "PK_e032310bcef831fb83101899b10" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "admin"`);
    }

}
