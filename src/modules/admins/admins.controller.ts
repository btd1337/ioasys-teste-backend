import { Controller } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { AdminsService } from './admins.service';
import { CreateAdminDto } from './dto/create-admin.dto';
import { UpdateAdminDto } from './dto/update-admin.dto';
import { Admin } from './entities/admin.entity';

@Crud({
	model: {
		type: Admin,
	},
	dto: {
		create: CreateAdminDto,
		update: UpdateAdminDto,
	},
	query: {
		filter: {
			isDeleted: {
				$eq: false,
			},
		},
	},
})
@Controller('admins')
@ApiTags('admins')
@ApiBearerAuth()
export class AdminsController implements CrudController<Admin> {
	constructor(public service: AdminsService) {}
}
