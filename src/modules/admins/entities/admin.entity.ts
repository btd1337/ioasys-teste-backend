import { UseGuards } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import {
	UUID,
	MAX_NAME_LENGTH,
	MIN_NAME_LENGTH,
	MAX_LAST_NAME_LENGTH,
	MIN_LAST_NAME_LENGTH,
	MAX_EMAIL_LENGTH,
	MIN_EMAIL_LENGTH,
	MAX_USER_PASSWORD_LENGTH,
	MIN_USER_PASSWORD_LENGTH,
} from 'src/config/constants';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { AuthGuard } from '@nestjs/passport';

@Entity()
@UseGuards(AuthGuard('Jwt'))
export class Admin {
	@PrimaryGeneratedColumn('uuid')
	@ApiProperty(UUID)
	id: string;

	@Column({ type: 'varchar', length: MAX_NAME_LENGTH })
	@ApiProperty({
		type: String,
		minLength: MIN_NAME_LENGTH,
		maxLength: MAX_NAME_LENGTH,
	})
	name: string;

	@Column({ type: 'varchar', length: MAX_LAST_NAME_LENGTH })
	@ApiProperty({
		type: String,
		minLength: MIN_LAST_NAME_LENGTH,
		maxLength: MAX_LAST_NAME_LENGTH,
	})
	lastName: string;

	@Column({ type: 'varchar', unique: true, length: MAX_EMAIL_LENGTH })
	@ApiProperty({ type: String, minLength: MIN_EMAIL_LENGTH, maxLength: MAX_EMAIL_LENGTH })
	email: string;

	@Column({ type: 'varchar' })
	@Exclude()
	salt: string;

	@Column({ type: 'varchar' })
	@ApiProperty({
		type: String,
		minLength: MIN_USER_PASSWORD_LENGTH,
		maxLength: MAX_USER_PASSWORD_LENGTH,
	})
	@Exclude({ toPlainOnly: true })
	password: string;

	@Column({ type: 'boolean', default: false })
	@Exclude()
	isDeleted: boolean;

	@Exclude()
	roles: string[] = ['admin'];

	constructor(partial: Partial<Admin>) {
		Object.assign(this, partial);
	}
}
