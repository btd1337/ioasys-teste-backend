import { Body, Controller, Post, ValidationPipe } from '@nestjs/common';
import {
	ApiOperation,
	ApiOkResponse,
	ApiUnauthorizedResponse,
	ApiNotFoundResponse,
	ApiTags,
} from '@nestjs/swagger';
import {
	notFoundMessage,
	unauthorizedCredentialsMessage,
} from 'src/utils/messages/default.message';
import { AuthService } from './auth.service';
import { SignInResponseDto } from './dto/signin-response.dto';
import { SignInDto } from './dto/signin.dto';

@Controller('auth')
@ApiTags('auth')
export class AuthController {
	constructor(private service: AuthService) {}

	@Post('users/signin')
	@ApiOperation({ summary: 'User Login' })
	@ApiOkResponse({
		description: 'accessToken: string; id: string',
	})
	@ApiUnauthorizedResponse({
		description: unauthorizedCredentialsMessage,
	})
	@ApiNotFoundResponse({
		description: notFoundMessage('User'),
	})
	async signInUser(@Body(ValidationPipe) dto: SignInDto): Promise<SignInResponseDto> {
		return this.service.signInUser(dto);
	}

	@Post('admins/signin')
	@ApiOperation({ summary: 'Admin Login' })
	@ApiOkResponse({
		description: 'accessToken: string; id: string',
	})
	@ApiUnauthorizedResponse({
		description: unauthorizedCredentialsMessage,
	})
	@ApiNotFoundResponse({
		description: notFoundMessage('Admin'),
	})
	async signInAdmin(@Body(ValidationPipe) dto: SignInDto): Promise<SignInResponseDto> {
		return this.service.signInAdmin(dto);
	}
}
