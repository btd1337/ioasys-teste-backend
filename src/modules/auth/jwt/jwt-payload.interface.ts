import { UserType } from 'src/utils/enums/user-type.enum';

export interface JwtPayload {
	email: string;
	userType: UserType;
}
