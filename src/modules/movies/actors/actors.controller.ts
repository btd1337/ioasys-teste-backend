import { Controller, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/utils/guards/roles.guard';
import { CreateActorDto } from '../dto/create-actor.dto';
import { UpdateActorDto } from '../dto/update-actor.dto';
import { Actor } from '../entities/actor.entity';
import { ActorsService } from './actors.service';

@Crud({
	model: {
		type: Actor,
	},
	dto: {
		create: CreateActorDto,
		update: UpdateActorDto,
	},
})
@Controller('actors')
@UseGuards(
	AuthGuard('jwt'),
	new RolesGuard({
		createOneRoles: ['admin'],
		updateOneRoles: ['admin'],
		deleteOneRoles: ['admin'],
	}),
)
@ApiTags('actors')
@ApiBearerAuth()
export class ActorsController implements CrudController<Actor> {
	constructor(public service: ActorsService) {}
}
