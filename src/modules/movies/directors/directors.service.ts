import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';
import { Director } from '../entities/director.entity';

@Injectable()
export class DirectorsService extends TypeOrmCrudService<Director> {
	constructor(@InjectRepository(Director) repository: Repository<Director>) {
		super(repository);
	}
}
