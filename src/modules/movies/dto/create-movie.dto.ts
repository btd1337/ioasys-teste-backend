import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsString, IsUUID, Length } from 'class-validator';
import { MIN_NAME_LENGTH, MAX_NAME_LENGTH, UUID } from 'src/config/constants';
import { Genre } from 'src/utils/enums/genre.enum';
import { IdPartialObject } from 'src/utils/id-partial-object.class';
import { Actor } from '../entities/actor.entity';

export class CreateMovieDto {
	@ApiProperty({
		type: String,
		minLength: MIN_NAME_LENGTH,
		maxLength: MAX_NAME_LENGTH,
	})
	@IsString()
	@Length(MIN_NAME_LENGTH, MAX_NAME_LENGTH)
	@IsNotEmpty()
	name: string;

	@ApiProperty({
		type: Genre,
		enum: Genre,
	})
	@IsEnum(Genre)
	@IsNotEmpty()
	genre: Genre;

	@ApiProperty(UUID)
	@IsUUID()
	@IsNotEmpty()
	director: string;

	@ApiProperty({
		type: () => [IdPartialObject],
		example: [
			{ id: '2d1b0e9a-ddc1-403e-99ac-e265d02af2eb' },
			{ id: 'dd8d3331-cb42-4251-96cd-b13909c3fd90' },
		],
	})
	@IsNotEmpty()
	actors: Actor[];
}
