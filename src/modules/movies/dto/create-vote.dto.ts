import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsUUID, Max, Min } from 'class-validator';
import { MAX_MOVIE_RATING, MIN_MOVIE_RATING, UUID } from 'src/config/constants';

export class CreateVoteDto {
	@ApiProperty(UUID)
	@IsUUID()
	@IsNotEmpty()
	user: string;

	@ApiProperty(UUID)
	@IsUUID()
	@IsNotEmpty()
	movie: string;

	@ApiProperty({
		type: 'number',
		minimum: MIN_MOVIE_RATING,
		maximum: MAX_MOVIE_RATING,
	})
	@IsNotEmpty()
	@IsInt()
	@Min(MIN_MOVIE_RATING)
	@Max(MAX_MOVIE_RATING)
	rating: number;
}
