import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsString, Length } from 'class-validator';
import { MAX_NAME_LENGTH, MIN_NAME_LENGTH } from 'src/config/constants';
import { Genre } from 'src/utils/enums/genre.enum';

export class UpdateMovieDto {
	@ApiProperty({
		type: String,
		minLength: MIN_NAME_LENGTH,
		maxLength: MAX_NAME_LENGTH,
		required: false,
	})
	@IsString()
	@Length(MIN_NAME_LENGTH, MAX_NAME_LENGTH)
	@IsNotEmpty()
	name?: string;

	@ApiProperty({
		type: Genre,
		enum: Genre,
		required: false,
	})
	@IsEnum(Genre)
	@IsNotEmpty()
	genre?: Genre;
}
