import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsUUID, Max, Min } from 'class-validator';
import { UUID, MIN_MOVIE_RATING, MAX_MOVIE_RATING } from 'src/config/constants';

export class UpdateVoteDto {
	@ApiProperty(UUID)
	@IsUUID()
	@IsNotEmpty()
	user: string;

	@ApiProperty(UUID)
	@IsUUID()
	@IsNotEmpty()
	movie: string;

	@ApiProperty({
		type: 'number',
		minimum: MIN_MOVIE_RATING,
		maximum: MAX_MOVIE_RATING,
	})
	@IsNotEmpty()
	@IsInt()
	@Min(MIN_MOVIE_RATING)
	@Max(MAX_MOVIE_RATING)
	rating: number;
}
