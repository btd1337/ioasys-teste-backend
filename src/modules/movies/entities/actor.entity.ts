import { ApiProperty } from '@nestjs/swagger';
import { MAX_NAME_LENGTH, MIN_NAME_LENGTH, UUID } from 'src/config/constants';
import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Movie } from './movie.entity';

@Entity()
export class Actor {
	@PrimaryGeneratedColumn('uuid')
	@ApiProperty(UUID)
	id: string;

	@Column({ type: 'varchar', length: MAX_NAME_LENGTH })
	@ApiProperty({
		type: String,
		minLength: MIN_NAME_LENGTH,
		maxLength: MAX_NAME_LENGTH,
	})
	name: string;

	@ManyToMany(() => Movie, (movie) => movie.actors)
	movies: Movie[];
}
