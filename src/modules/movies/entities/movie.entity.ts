import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { UUID, MAX_NAME_LENGTH, MIN_NAME_LENGTH, DECIMAL_PLACES } from 'src/config/constants';
import { Genre } from 'src/utils/enums/genre.enum';
import {
	Column,
	Entity,
	JoinTable,
	ManyToMany,
	ManyToOne,
	OneToMany,
	PrimaryGeneratedColumn,
} from 'typeorm';
import { Actor } from './actor.entity';
import { Director } from './director.entity';
import { Vote } from './vote.entity';

@Entity()
export class Movie {
	@PrimaryGeneratedColumn('uuid')
	@ApiProperty(UUID)
	id: string;

	@Column({ type: 'varchar', length: MAX_NAME_LENGTH })
	@ApiProperty({
		type: String,
		minLength: MIN_NAME_LENGTH,
		maxLength: MAX_NAME_LENGTH,
	})
	name: string;

	@Column({ type: 'enum', enum: Genre })
	@ApiProperty({ type: Genre, enum: Genre })
	genre: Genre;

	@ManyToMany(() => Actor, (actor) => actor.movies)
	@JoinTable()
	@ApiProperty({ type: [Actor] })
	actors: Actor[];

	@ManyToOne(() => Director, (director) => director.movies)
	@ApiProperty({ type: Director })
	director: Director;

	@OneToMany(() => Vote, (vote) => vote.movie)
	@Exclude()
	votes: Vote[];

	@Expose()
	get rating(): string {
		let ratingSum = 0;

		// necessary for relation to other tables
		if (!this.votes) {
			return undefined;
		}

		if (this.votes.length === 0) {
			return '0.00';
		}

		for (let i = 0; i < this.votes.length; i++) {
			ratingSum += this.votes[i].rating;
		}

		return (ratingSum / this.votes.length).toFixed(DECIMAL_PLACES);
	}
}
