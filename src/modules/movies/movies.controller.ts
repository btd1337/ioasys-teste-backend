import { Controller, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { RolesGuard } from 'src/utils/guards/roles.guard';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';
import { Movie } from './entities/movie.entity';
import { MoviesService } from './movies.service';

@Crud({
	model: {
		type: Movie,
	},
	dto: {
		create: CreateMovieDto,
		update: UpdateMovieDto,
	},
	query: {
		join: {
			director: {
				eager: true,
			},
			actors: {
				eager: true,
			},
			votes: {
				eager: true,
			},
		},
	},
})
@Controller('movies')
@UseGuards(
	AuthGuard('jwt'),
	new RolesGuard({
		createOneRoles: ['admin'],
		updateOneRoles: ['admin'],
		deleteOneRoles: ['admin'],
	}),
)
@ApiTags('movies')
@ApiBearerAuth()
export class MoviesController implements CrudController<Movie> {
	constructor(public service: MoviesService) {}
}
