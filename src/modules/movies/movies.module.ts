import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MoviesService } from './movies.service';
import { MoviesController } from './movies.controller';
import { DirectorsController } from './directors/directors.controller';
import { DirectorsService } from './directors/directors.service';
import { ActorsService } from './actors/actors.service';
import { ActorsController } from './actors/actors.controller';
import { Actor } from './entities/actor.entity';
import { Director } from './entities/director.entity';
import { Movie } from './entities/movie.entity';
import { VotesService } from './votes/votes.service';
import { VotesController } from './votes/votes.controller';
import { Vote } from './entities/vote.entity';

@Module({
	imports: [TypeOrmModule.forFeature([Actor, Director, Movie, Vote])],
	providers: [MoviesService, DirectorsService, ActorsService, VotesService],
	exports: [ActorsService, DirectorsService, MoviesService, VotesService],
	controllers: [MoviesController, DirectorsController, ActorsController, VotesController],
})
export class MoviesModule {}
