import { Controller, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { RolesGuard } from 'src/utils/guards/roles.guard';
import { CreateVoteDto } from '../dto/create-vote.dto';
import { UpdateVoteDto } from '../dto/update-vote.dto';
import { Vote } from '../entities/vote.entity';
import { VotesService } from './votes.service';

@Crud({
	model: {
		type: Vote,
	},
	dto: {
		create: CreateVoteDto,
		update: UpdateVoteDto,
	},
	query: {
		join: {
			user: { eager: true },
			movie: { eager: true },
		},
	},
})
@Controller('votes')
@UseGuards(
	AuthGuard('jwt'),
	new RolesGuard({
		createOneRoles: ['user'],
	}),
)
@ApiTags('votes')
@ApiBearerAuth()
export class VotesController implements CrudController<Vote> {
	constructor(public service: VotesService) {}
}
