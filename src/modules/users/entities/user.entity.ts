import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import {
	MAX_EMAIL_LENGTH,
	MAX_LAST_NAME_LENGTH,
	MAX_NAME_LENGTH,
	MAX_USER_PASSWORD_LENGTH,
	MIN_EMAIL_LENGTH,
	MIN_LAST_NAME_LENGTH,
	MIN_NAME_LENGTH,
	MIN_USER_PASSWORD_LENGTH,
	UUID,
} from 'src/config/constants';
import { Exclude } from 'class-transformer/decorators';
import { Vote } from 'src/modules/movies/entities/vote.entity';

@Entity()
export class User {
	@PrimaryGeneratedColumn('uuid')
	@ApiProperty(UUID)
	id: string;

	@Column({ type: 'varchar', length: MAX_NAME_LENGTH })
	@ApiProperty({
		type: String,
		minLength: MIN_NAME_LENGTH,
		maxLength: MAX_NAME_LENGTH,
	})
	name: string;

	@Column({ type: 'varchar', length: MAX_LAST_NAME_LENGTH })
	@ApiProperty({
		type: String,
		minLength: MIN_LAST_NAME_LENGTH,
		maxLength: MAX_LAST_NAME_LENGTH,
	})
	lastName: string;

	@Column({ type: 'varchar', unique: true, length: MAX_EMAIL_LENGTH })
	@ApiProperty({ type: String, minLength: MIN_EMAIL_LENGTH, maxLength: MAX_EMAIL_LENGTH })
	email: string;

	@Column({ type: 'varchar' })
	@ApiProperty({
		type: String,
		minLength: MIN_USER_PASSWORD_LENGTH,
		maxLength: MAX_USER_PASSWORD_LENGTH,
	})
	@Exclude({ toPlainOnly: true })
	password: string;

	@Column({ type: 'boolean', default: false })
	@Exclude()
	isDeleted: boolean;

	@OneToMany(() => Vote, (vote) => vote.user)
	votes: Vote[];

	@Column({ type: 'varchar' })
	@Exclude()
	salt: string;

	@Exclude()
	roles: string[] = ['user'];

	constructor(partial: Partial<User>) {
		Object.assign(this, partial);
	}
}
