import { Controller } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { UsersService } from './users.service';

@Crud({
	model: {
		type: User,
	},
	dto: {
		create: CreateUserDto,
		update: UpdateUserDto,
	},
	query: {
		filter: {
			isDeleted: {
				$eq: false,
			},
		},
	},
})
@Controller('users')
@ApiTags('users')
@ApiBearerAuth()
export class UsersController implements CrudController<User> {
	constructor(public service: UsersService) {}
}
