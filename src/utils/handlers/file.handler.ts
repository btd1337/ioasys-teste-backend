import * as fs from 'fs';
import { HttpException, HttpStatus } from '@nestjs/common';
import { Response } from 'express';

export async function IsFileExists(path: string): Promise<boolean> {
	return fs.existsSync(path);
}

export const sendFileByResponse = async (
	res: Response,
	filename: string,
	folderPath: string,
): Promise<void> => {
	res.sendFile(filename, { root: `${folderPath}` }, (error) => {
		if (error) {
			throw new HttpException(error, HttpStatus.EXPECTATION_FAILED);
		}
	});
};
