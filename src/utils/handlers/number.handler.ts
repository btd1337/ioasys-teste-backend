import * as bcrypt from 'bcryptjs';

export async function validatePassword(
	password: string,
	encryptedPassword: string,
	salt: string,
): Promise<boolean> {
	const hash = await bcrypt.hash(password, salt);
	return hash === encryptedPassword;
}
