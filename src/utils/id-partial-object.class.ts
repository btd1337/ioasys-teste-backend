import { ApiProperty } from '@nestjs/swagger';
import { UUID } from 'src/config/constants';

export class IdPartialObject {
	@ApiProperty(UUID)
	id: string;
}
