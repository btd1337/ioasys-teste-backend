export const notFoundMessage = (objectName: string): string => `${objectName} not found`;

export const isRequiredMessage = (objectName: string): string => `${objectName} is required`;

export const invalidTypeMessage = (objectName: string): string => `${objectName} invalid`;

export const unauthorizedCredentialsMessage =
	'e-mail password you entered did not match our records. Please double-check and try again';
