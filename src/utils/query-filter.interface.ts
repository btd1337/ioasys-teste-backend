export interface QueryFilter {
	field: string;
	operator: string;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	value?: any;
}
